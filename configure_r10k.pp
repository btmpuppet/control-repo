class { 'r10k':
  sources => {
    'btmpuppet' => {
      'remote'  => 'ssh://git@bitbucket.org/btmpuppet/control-repo.git',
      'basedir' => "${::settings::confdir}/environments",
      'prefix'  => false,
    },
  }
}
